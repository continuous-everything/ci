import requests
import os
import sys

API_ENDPOINT = os.environ['GITLAB_API_ENDPOINT']
API_TOKEN = os.environ['GILATB_API_TOKEN']
project_name = sys.argv[1]

data_group = {
    "private_token": API_TOKEN,
    "name": project_name,
    "path": project_name,
    "visibility": "private",
    "parent_id": 2053
}

r = requests.post(url=API_ENDPOINT + "/groups", data=data_group)
print(r.text)
group_id = r.json()['id']

data_fork = {
    "private_token": API_TOKEN,
    "namespace": group_id,
}

r = requests.post(url=API_ENDPOINT + "/projects/1930/fork", data=data_fork)
print(r.text)

data_user = {
    "private_token": API_TOKEN,
}

r = requests.get(url=API_ENDPOINT + "/users?username=" + sys.argv[2],
                 data=data_user)
user_id = r.json()[0]['id']

data_member = {
    "private_token": API_TOKEN,
    "user_id": user_id,
    "access_level": 40
}

r = requests.post(url=API_ENDPOINT + "/groups/" + str(group_id) + "/members",
                  data=data_member)
print(r.text)
