from setuptools import setup, Extension


setup(
    name='add',
    version='1.0',
    description='Python Package for addition',
    ext_modules=[
        Extension(
            'add',
            sources=['addmodule.c'],
            py_limited_api=True)
    ],
)
