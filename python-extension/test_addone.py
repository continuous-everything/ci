import unittest
import add


class TestAddOne(unittest.TestCase):

    def test_int_add_one(self):
        self.assertEqual(add.add_one(0), 1)
