.. dice documentation master file, created by
   sphinx-quickstart on Mon Aug 27 15:54:19 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dice's documentation!
================================


.. raw:: html

   <div> placeholder for badges </div>

Documentation très complète de dice

.. toctree::
   :maxdepth: 2

.. automodule:: dice
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
