import unittest
import dice


class TestDice(unittest.TestCase):

    def test_dice_roll(self):
        self.assertEqual(len(dice.roll(6, 6)), 6)

    def test_dice_dice(self):
        for result in (dice.roll(6,6)):
            self.assertGreaterEqual(result, 1)
            self.assertLessEqual(result, 6)
