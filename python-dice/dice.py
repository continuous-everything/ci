import random

""" This is a library rolling dices.
"""


def roll(dices, faces):
    """ Roll dices !

    Parameters
    ----------
        dices: number of dices to be rolled

        faces: number of dice faces

    Returns
    -------
        result: int array
            value for each dice


    """
    return [random.randint(1, faces) for i in range(0, dices)]
