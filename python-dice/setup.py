from setuptools import setup, Extension


setup(
    name='dice-ci',
    version='0.1',
    description='Python Package for dicing',
    py_modules=['dice'],
)
